
 const request = (token, url, method, body, headers) => {

    let newData = JSON.stringify(body)
    
    let requestHeaders = {
        'Content-Type': 'application/json',
        "authorization": token
    }

   return fetch(url, {
            method,
            body: newData,
            headers: {
                ...requestHeaders
            }
        }).then(response => response.json())
     
}   

const authorization = (tokenAuth, url, loginValue, passwordValue, cb) => {
    request(tokenAuth, url, "POST", {   
                email: loginValue,
                password: passwordValue,
            })
            .then(response => {
                if (response.statusCode !== 400) {
                    tokenAuth = response.token
                    localStorage.setItem("token", response.token)
                    if (cb) {
                        cb()
                    }
                } else if (response.statusCode === 400) {
                    alert(response.message)
                } 
            })
      
};


export {request}
export {authorization}