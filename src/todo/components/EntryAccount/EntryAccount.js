import React from "react";
import { Link } from "react-router-dom";

const styles = {
    div: {
        display: 'flex',
        marginTop: "4vh",
        alignItems: 'center'
    },
  img: {
    marginRight: "1vh",  
    height: '3.5vh',
    width: '3.5vh'
  },
  name: {
    color: 'black',
    textDecoration: 'none'

  }
}

export const EntruAccount = () =>  {

    return (
    <div style={styles.div}>
         <img style={styles.img} src='http://cdn.onlinewebfonts.com/svg/img_243887.png' alt="account"></img>
         <Link style={styles.name} to="/Account" ><strong>User</strong></Link>
    </div>
    )
  }

