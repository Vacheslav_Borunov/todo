
import React, { useState } from "react";
import { request } from "../../../apiServ";
let tokenAuth = localStorage.getItem('token');


const styles = {
  button: {
      marginLeft: '1rem',
      border: '1px solid #ccc',
      borderRadius: '4px'
  }
}
const TodoInput = ({ setTodos }) => {
  const [value, setValue] = useState('')

  const submitHandler = () => {
    if (value.trim()) {
      let tokenAuth = localStorage.getItem('token');
        request(tokenAuth, 'http://localhost:3005/todo', "POST", {   
        body: value,
    }).then((res)=>{setTodos(prev => {
     return [...prev, res]})})

     
      setValue('')
    
    } else alert('Эти пробелы тут явно лишние)')
  }

  
    return (
      <div className="wrapper">
        <input type='text' className='form-control' value={value} onChange={event => setValue(event.target.value)}/>
        <button type="submit" onClick={submitHandler} style={styles.button}>Add</button>
      </div>
    )
  }


  // TodoInput.propTypes = {
  //   onCreate: PropTypes.func.isRequired
  // }

  export {TodoInput}