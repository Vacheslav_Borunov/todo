import React, {useEffect, useState} from "react";
import {request} from "../apiServ"
import { Link } from "react-router-dom";
import s from "../Account.module.css"
// import set from 'lodash/set'
const Account = () => {
    const [token, setToken] = useState('')
    // const [name, setName] = useState('')
    // const [age, setAge] = useState('')
    // const [email, setEmail] = useState('')
    const [pass, setPass] = useState('')
    const [newPass, setNewPass] = useState('')

    const [userInfo, setUserInfo] = useState({
        name: '',
        age: '',
        email: ''
    })

let tokenAuth = localStorage.getItem('token');

    useEffect ( async () => {
        let tokenAuth = localStorage.getItem('token');
        setToken(tokenAuth)
            const profile = await request(tokenAuth, 'http://localhost:3005/profile/me', "GET") 
                setUserInfo({name: profile.name, age: profile.age, email: profile.email})
                
    }, [])

     
    const changeData = async  () => {
         await request(token, 'http://localhost:3005/profile/me', "PATCH", {
            name: userInfo.name,
            age: userInfo.age, 
            email: userInfo.email 
        });
       };

       const changePass = async () => {
        await  request(tokenAuth, 'http://localhost:3005/profile/me/password', "PATCH", {
            oldPassword: pass, 
            newPassword: newPass 
        });
       }

      const handleInputChange = (e) => {
        setUserInfo( (prev) =>( {...prev, 
            [e.target.name]: e.target.value
        }));
    }
      

    return (
        <div className={s.wrapper}>
            <div className={s.user_heading}>
                <strong>ЛИЧНЫЙ КАБИНЕТ</strong>
                <img className={s.img} src='http://cdn.onlinewebfonts.com/svg/img_243887.png' alt="user"></img>
            </div>
               
            <div className={s.user_info}>
                <div className={s.user_info_data}>
                    <p className={s.lable}>ОСНОВНАЯ ИНФОРМАЦИЯ</p>
                    <p className={s.lable}>Имя</p>
                    <input value={userInfo.name} name="name" onChange={handleInputChange} className={s.imputs} type="text" ></input>
                    <p className={s.lable}>Возраст</p>
                    <input value={userInfo.age} name="age" className={s.imputs} type="text"  onChange={handleInputChange}></input>
                    <p className={s.lable}>Email</p>
                    <input value={userInfo.email} name="email" className={s.imputs} type="text"  onChange={handleInputChange}></input>
                    <button onClick={changeData}  className={s.btn_change}>Изменить данные</button>
                </div>
                <div className={s.user_pass_data}>
                    <p className={s.lable}>СМЕНА ПАРОЛЯ</p>
                    <p className={s.lable}>Введите старый пароль</p>
                    <input value={pass} onChange={event => setPass(event.target.value)} className={s.imputs}  type="password" ></input>
                    <p className={s.lable}>А тут новый!</p>
                    <input value={newPass} onChange={event => setNewPass(event.target.value)} className={s.imputs} type="password" ></input>
                    <button onClick={changePass} className={s.btn_change}>Изменить пароль</button>
                </div>
            </div>
           
            <Link to="/home">
                <button className={s.btn_change}>EXIT</button>
            </Link>  
         </div>
    )
}

export {Account}