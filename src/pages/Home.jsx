import '../App.css'
import React, { useState, useEffect} from 'react'
import { Link } from 'react-router-dom';
import Context from '../context'
import { TodoTitle } from '../todo/components/TodoTitle';
import { TodoInput } from '../todo/components/TodoInput';
import {request} from "../apiServ"
import { EntruAccount } from '../todo/components/EntryAccount/EntryAccount';

const classes = [];
const styles = {
  li: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '.5rem 1rem',
    border: '1px solid #ccc',
    borderRadius: '4px',
    marginBottom: '.5rem'
  },
  input: {
    marginRight: '1rem'
  },
  strong: {
    marginRight: '.2rem'
  },
  top: {
    display: 'flex',
    alignItems: 'end',
    justifyContent: 'space-between'
  },
  btn: {
      marginLeft: '1rem',
      border: '1px solid #ccc',
      borderRadius: '4px'
  }
 
}

const Home = () => {
    const [todos, setTodos] = useState([])
    const [token, setToken] = useState('')

    useEffect( async () => {
        let tokenAuth = localStorage.getItem('token');
        setToken(tokenAuth)
         const todos = await request(tokenAuth, 'http://localhost:3005/todo/list', "GET"
         )
         if (todos.statusCode === 404){
        setTodos([])
         } else {
           setTodos(todos)
        }
      
    }, [])
    
  const toggleTodo = async (id, status) => {
     await request(token, `http://localhost:3005/todo/${id}`, "PATCH", {   
        status : status === "PENDING" ? "DONE" : "PENDING"
        
    })
    setTodos(
      todos.map(todo => {
      if (todo.id === id) {
        todo.status = todo.status === "PENDING" ? "DONE" : "PENDING"
      }
      return todo
    }
    )
    
    )
  }
 
 

  const removeTodo = (id) => {
    setTodos(todos.filter(todo => todo.id !== id))
    request(token, `http://localhost:3005/todo/${id}`, "DELETE")//проверка
    
  }

  
    return (
    <Context.Provider value={{removeTodo}}>
        
        <div className="App container">
            <div style={styles.top}>
              <EntruAccount />
              <Link to="/">
                  <button style={styles.btn}>Exit</button>
              </Link>  
            </div>
            <TodoTitle />
            <TodoInput  setTodos={setTodos}/>
            
              { todos.length ? (
                <ul>
                {todos.map((el) => (
                      <li key={el.id} style={styles.li}>
                        <span className={classes.join(' ')}>
                          <input type='checkbox' style={styles.input} defaultChecked={el.status === "PENDING" ? false : true} onClick={() => toggleTodo(el.id, el.status) } />
                          {el.body} 
                        </span>
                        <button className="rm" onClick={() => removeTodo(el.id)}>&times;</button>
                      </li>
                  )
                )}
                </ul>
                ) : (
                    <p >Тут пусто, но ты можешь это исправить!</p>
                )}
            
        </div>
    </Context.Provider>
    )
}


export {Home}