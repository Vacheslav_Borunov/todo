
import React, { useState } from "react";
import { authorization } from "../apiServ";
import '../Entry.css'
import { useNavigate } from "react-router-dom";
let tokenAuth = localStorage.getItem('token');

const Entry = (props) => {
        const [userParams, setUserParams] = useState({
            loginValue: '',
            passwordValue: ''
        })

        let navigate = useNavigate();

     
    const cb = () => {
        navigate("/home");
       
    }
    return (
        <div className={"wrapper_entru"}>
            <p className={"text_top"}>ВХОД В ЛИЧНЫЙ КАБИНЕТ</p>
            <input type="email" value={userParams.loginValue} onChange={(e) => setUserParams({
        loginValue: e.target.value,
        passwordValue: userParams.passwordValue
    })} className={"email"}></input>
            <input type="password" value={userParams.passwordValue} onChange={(e) => setUserParams({
        loginValue: userParams.loginValue,
        passwordValue: e.target.value
    })} className={"password"}></input>
        
                <button onClick={() => {authorization(tokenAuth, 'http://localhost:3005/auth/sign-in', userParams.loginValue, userParams.passwordValue, cb)}} className={"btn_entry"}>Вход</button>
         
                <button onClick={() => {authorization(tokenAuth, 'http://localhost:3005/auth/sign-up', userParams.loginValue, userParams.passwordValue, cb)}} className={"btn_registration"}>Регистрация</button>
 
        </div>
    )
}

export {Entry}