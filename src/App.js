
import { Entry } from './pages/Entry';
import { Home } from './pages/Home';
import { ProtectedRoute } from './pages/Protected';
import React from 'react';
import { Routes, Route } from 'react-router-dom';
import { Account } from './pages/Account';
import './App.css'

const NotFound = () => {
  return  (
    <div>
      <strong className="App-header">404
        <p>Что-то пошло не так(</p>
      </strong>
    </div>
  )
}
const App = () => {
  return (
      <Routes>
        <Route path="/" element={<ProtectedRoute component={Entry} />} />
        <Route path="/home" element={<ProtectedRoute component={Home} />} />
        <Route path='/account' element={<ProtectedRoute component={Account} />} />
        <Route path="*" element={<ProtectedRoute component={NotFound} /> } />
      </Routes>
  );
  
}

export default App;
   